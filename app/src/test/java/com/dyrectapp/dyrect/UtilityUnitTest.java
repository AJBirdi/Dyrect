package com.dyrectapp.dyrect;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class UtilityUnitTest {

    @Test
    public void TestFormatFullGregorianToString() {
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.DAY_OF_MONTH, 20);
        calendar.set(Calendar.MONTH, 5);

        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 30);

        String formatted = Utility.formatFullGregorianToString(calendar, false);
        String displayFormatted = Utility.formatFullGregorianToString(calendar, true);

        Assert.assertEquals(formatted, "05/20/2017 10:30");
        Assert.assertEquals(displayFormatted, "06/20/2017 10:30 AM");
    }

    @Test
    public void TestFormatStringToGregorian() {
        String formatted = "05/20/2017 10:30";

        GregorianCalendar calendar = Utility.formatStringToGregorian(formatted);

        Assert.assertEquals(calendar.get(Calendar.MONTH), 5);
        Assert.assertEquals(calendar.get(Calendar.DAY_OF_MONTH), 20);
        Assert.assertEquals(calendar.get(Calendar.YEAR), 2017);

        Assert.assertEquals(calendar.get(Calendar.HOUR_OF_DAY), 10);
        Assert.assertEquals(calendar.get(Calendar.MINUTE), 30);
    }

    @Test
    public void TestFormatDateGregorianToString() {
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.MONTH, 5);
        calendar.set(Calendar.DAY_OF_MONTH, 20);

        String formatted = Utility.formatDateGregorianToString(calendar);
        Assert.assertEquals(formatted, "05/20/2017 00:00");
    }

    @Test
    public void TestFormatTimeGregorianToString() {
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 30);

        String formatted = Utility.formatTimeGregorianToString(calendar);
        Assert.assertEquals(formatted, "00/01/1970 10:30");
    }

    @Test
    public void TestIsolateTime() {
        String formatted = "05/20/2017 10:30";

        String isolatedTime = Utility.isolateTime(formatted);
        Assert.assertEquals(isolatedTime, "10:30");
    }

    @Test
    public void TestIsolateDate() {
        String formatted = "05/20/2017 10:30";

        String isolatedDate = Utility.isolateDate(formatted);
        Assert.assertEquals(isolatedDate, "05/20/2017");
    }

    @Test
    public void TestIsolateHour () {
        String fullDate = "05/20/2017 10:30";
        String isolatedTime = Utility.isolateTime(fullDate);
        int isolatedHour = Utility.isolateHour(isolatedTime);

        Assert.assertEquals(isolatedHour, 10);
    }

    @Test
    public void TestIsolateMinute() {
        String fullDate = "05/20/2017 10:30";
        String isolatedTime = Utility.isolateTime(fullDate);
        int isolatedMinute = Utility.isolateMinute(isolatedTime);

        Assert.assertEquals(isolatedMinute, 30);
    }

    @Test
    public void TestIsolateMonth() {
        String fullDate = "05/20/2017 10:30";
        String isolatedDate = Utility.isolateDate(fullDate);
        int isolatedMonth = Utility.isolateMonth(isolatedDate);

        Assert.assertEquals(isolatedMonth, 5);
    }

    @Test
    public void TestIsolateDay() {
        String fullDate = "05/20/2017 10:30";
        String isolatedDate = Utility.isolateDate(fullDate);
        int isolatedDay = Utility.isolateDay(isolatedDate);

        Assert.assertEquals(isolatedDay, 20);
    }

    @Test
    public void TestIsolateYear() {
        String fullDate = "05/20/2017 10:30";
        String isolatedDate = Utility.isolateDate(fullDate);
        int isolatedYear = Utility.isolateYear(isolatedDate);

        Assert.assertEquals(isolatedYear, 2017);
    }

    @Test
    public void TestGetDisplayHourWhereHourEquals0() {
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.DAY_OF_MONTH, 20);
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.MONTH, 5);

        String formatted = Utility.formatFullGregorianToString(calendar, true);
        Assert.assertEquals(formatted, "06/20/2017 12:30 AM");
    }

    @Test
    public void TestGetDisplayHourWhereHourEquals13() {
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.set(Calendar.HOUR_OF_DAY, 13);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.DAY_OF_MONTH, 20);
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.MONTH, 5);

        String formatted = Utility.formatFullGregorianToString(calendar, true);
        Assert.assertEquals(formatted, "06/20/2017 01:30 PM");
    }

    @Test
    public void TestGetDisplayHourWhereHourEquals15() {
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.set(Calendar.HOUR_OF_DAY, 15);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.DAY_OF_MONTH, 20);
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.MONTH, 5);

        String formatted = Utility.formatFullGregorianToString(calendar, true);
        Assert.assertEquals(formatted, "06/20/2017 03:30 PM");
    }

    @Test
    public void TestFormatFullGregorianWhereMonthGreaterThan10() {
        GregorianCalendar calendar = new GregorianCalendar();

        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.DAY_OF_MONTH, 20);
        calendar.set(Calendar.YEAR, 2017);
        calendar.set(Calendar.MONTH, 11);

        String formatted = Utility.formatFullGregorianToString(calendar, false);
        Assert.assertEquals(formatted, "11/20/2017 10:30");
    }

    @Test
    public void TestFormatForTimeDisplay() {
        String formatted = "05/20/2017 10:30 AM";
        String formattedForTime = Utility.formatForTimeDisplay(formatted);

        Assert.assertEquals(formattedForTime, "10:30 AM");
    }

    @Test
    public void TestFormatForDateDisplay() {
        String formatted = "05/20/2017 10:30 AM";
        String formattedForDate = Utility.formatForDateDisplay(formatted);

        Assert.assertEquals(formattedForDate, "05/20/2017");
    }
}
