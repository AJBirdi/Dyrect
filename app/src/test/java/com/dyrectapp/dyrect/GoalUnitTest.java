package com.dyrectapp.dyrect;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class GoalUnitTest {

    private Goal goal;

    @Before
    public void setUp() {
        goal = new Goal();

        goal.setGoalName("Goal");
        goal.setCategory("Work");
        goal.setDueDate(Utility.formatStringToGregorian("05/20/2017 10:30"));
        goal.setNotes("");
    }

    @Test
    public void TestGoalToString() {
        String toString = goal.toString();
        Assert.assertTrue(toString.contentEquals("Name: Goal, Category: Work, Due Date: 05/20/2017 10:30, Notes: "));
    }
}
