
package com.dyrectapp.dyrect;

import java.io.Serializable;
import java.util.GregorianCalendar;

class Goal implements
        Serializable {

    private int ID;

    private GregorianCalendar dueDate;

    private String goalName;
    private String category;
    private String notes;

    int getID() {
        return ID;
    }

    void setID(int ID) {
        this.ID = ID;
    }

    GregorianCalendar getDueDate() {
        return dueDate;
    }

    void setDueDate(GregorianCalendar dueDate) {
        this.dueDate = dueDate;
    }

    String getGoalName() {
        return goalName;
    }

    void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    String getCategory() {
        return category;
    }

    void setCategory(String category) {
        this.category = category;
    }

    String getNotes() {
        return notes;
    }

    void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        if (ID != 0) {
            builder.append("\nID: " + getID());
        }
        builder.append("Name: " + getGoalName() + ", ");
        builder.append("Category: " + getCategory() + ", ");
        builder.append("Due Date: " + Utility.formatFullGregorianToString(getDueDate(), false) + ", ");
        builder.append("Notes: " + getNotes());

        return builder.toString();
    }
}