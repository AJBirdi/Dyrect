package com.dyrectapp.dyrect;

import android.net.Uri;
import android.provider.BaseColumns;

import java.net.URI;

class GoalsContract {

    private GoalsContract() {}

    static class Goals implements BaseColumns {
        static final String TABLE_NAME = "goals";
        static final String COLUMN_NAME_GOAL_NAME = "goal_name";
        static final String COLUMN_NAME_DATE_AND_TIME = "date_and_time";
        static final String COLUMN_NAME_CATEGORY = "category";
        static final String COLUMN_NAME_NOTES = "notes";

        static final Uri CONTENT_URI = Uri.parse("content://" + DyrectProvider.AUTHORITY + "/" + TABLE_NAME);

        static final String SQL_CREATE_GOALS =
                "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY, " +
                COLUMN_NAME_GOAL_NAME + " TEXT NOT NULL, " +
                COLUMN_NAME_DATE_AND_TIME + " TEXT NOT NULL, " +
                COLUMN_NAME_CATEGORY + " TEXT NOT NULL, " +
                COLUMN_NAME_NOTES + " TEXT);" ;

        static final String SQL_DELETE_GOALS =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}