package com.dyrectapp.dyrect;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/*
Checklist to release
TODO 1: Testing
TODO 2: Clean up code/document
TODO 3: Publish

TODO 4: Add notifications
TODO 5: Create landscape mode
TODO 6: Make Set Time/Set Date buttons text views of the current set Time/Date
TODO 7: Allow subgoals
TODO 8: Proper redesign
*/

public class MainActivity extends AppCompatActivity {

    private final String TAG = "MAIN_ACTIVITY";
    static final String RESULT = "RESULT";
    static final String GOAL_TO_UPDATE = "GOAL_TO_UPDATE";
    private final String GOALS_TOOLBAR = "Goals";

    ArrayList<Goal> goalsList;
    static int CREATE_GOAL_REQUEST = 1;
    static int UPDATE_GOAL_REQUEST = 2;
    private GoalsAdapter adapter;
    //Adapter position is the position of a Goal inside the Recycler View adapter
    private int adapterPosition;

    /*
    Sets content view to activity_main layout

    Sets supportActionToolbar to the custom mainActivityToolbar

    Creates a RecyclerView and RecyclerView adapter, then gets a list of Goals from the Goals
    database

    Implements the updateGoal method of the RecyclerView adapter, attaches the adapter to the
    RecyclerView, then sets the layout manager for the RecyclerView
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //TODO Remove all unnecessary actions to run on a different thread
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mainActivityToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(GOALS_TOOLBAR);

        RecyclerView goalRecyclerView = (RecyclerView) findViewById(R.id.goalRecyclerView);

        goalsList = Utility.createGoalListFromDatabase(getApplicationContext());
        adapter = new GoalsAdapter(this, goalsList);
        adapter.setUpdateExistingGoalListener(new GoalsAdapter.UpdateExistingGoalListener() {
            @Override
            public void updateGoal(int adapterPosition) {
                int databasePosition = adapterPosition + 1;

                Uri specificUri = ContentUris.withAppendedId(GoalsContract.Goals.CONTENT_URI, databasePosition);
                Cursor cursor = getContentResolver().query(specificUri, null, null, null, null);

                launchUpdateActivity(cursor, databasePosition);
            }
        });

        goalRecyclerView.setAdapter(adapter);
        goalRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        final FloatingActionButton createFAB = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        goalRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    createFAB.show();
                }
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0 || dy < 0 && createFAB.isShown()) {
                    createFAB.hide();
                }
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    /*
    If the request code is CREATE_GOAL_REQUEST AND the result code is okay, then get the goal from
    the intent, add the goal to the RecyclerView list, and update the RecyclerView adapter

    If the request code is UPDATE_GOAL_REQUEST and the result code is okay, then get the goal from
    the intent, update the goal in the goal list, and update the RecyclerView adapter

    Else if the result code is RESULT_DELETE then remove the goal from the goal list and update the
    adapter
    */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == CREATE_GOAL_REQUEST) {
            if(resultCode == RESULT_OK) {
                Goal newGoal = (Goal) data.getSerializableExtra(MainActivity.RESULT);
                goalsList.add(goalsList.size(), newGoal);
                adapter.notifyItemInserted(goalsList.size());
            }
        }
        else if(requestCode == UPDATE_GOAL_REQUEST) {
            if (resultCode == RESULT_OK) {
                Goal updatedGoal = (Goal) data.getSerializableExtra(MainActivity.RESULT);

                goalsList.set(adapterPosition, updatedGoal);
                adapter.notifyItemChanged(adapterPosition);
            }
            else if (resultCode == PrimaryTaskActivity.RESULT_DELETE) {
                goalsList.remove(adapterPosition);
                adapter.notifyItemRemoved(adapterPosition);
            }
        }
    }

    /*
    Creates an Intent to start the PrimaryTaskActivity, then launches the PrimaryTaskActivity class
    with the purpose of getting a new goal
     */
    public void createTask(View view) {
        Intent createNewGoal = new Intent(this, PrimaryTaskActivity.class);
        startActivityForResult(createNewGoal, CREATE_GOAL_REQUEST);
    }

    /*
    Sets the instance variable adapterPosition to the databasePosition - 1, where the adapterPosition
    is the index inside the RecyclerView and the databasePosition is the index inside the database,
    since the database does not 0-index while the adapter does

    If the cursor that is passed in has an actual goal in it, then set goalToUpdate to that goal,
    then add that goal to the updateGoal intent

    Finally, launch the PrimaryTaskActivity with the purpose of updating a goal
     */
    public void launchUpdateActivity(Cursor itemToUpdate, int databasePosition) {
        adapterPosition = databasePosition - 1;

        Intent updateGoal = new Intent(this, PrimaryTaskActivity.class);
        Goal goalToUpdate = new Goal();

        if (itemToUpdate.moveToFirst()) {
            goalToUpdate = Utility.createGoalFromCursor(itemToUpdate);
        }
        updateGoal.putExtra(MainActivity.GOAL_TO_UPDATE, goalToUpdate);
        startActivityForResult(updateGoal, UPDATE_GOAL_REQUEST);
    }
}