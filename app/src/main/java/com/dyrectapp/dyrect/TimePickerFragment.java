package com.dyrectapp.dyrect;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private SaveTimeListener callback;
    private final String TAG = "TIME_PICKER_FRAGMENT";

    interface SaveTimeListener {
        void onTimeSave(int hour, int minute);
    }

    public TimePickerFragment() {}

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int hour = 12;
        int minute = 0;

        Bundle arguments = getArguments();

        if(arguments != null) {
            String timeToUpdate;

            if (arguments.get(PrimaryTaskActivity.TIME_TO_LOAD) != null) {
                timeToUpdate = (String) arguments.get(PrimaryTaskActivity.TIME_TO_LOAD);
            }
            else {
                timeToUpdate = (String) arguments.get(PrimaryTaskActivity.TIME_TO_UPDATE);
            }
            hour = Utility.isolateHour(timeToUpdate);
            minute = Utility.isolateMinute(timeToUpdate);
        }
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;

        try {
            callback = (SaveTimeListener) activity;
        }
        catch(ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement SaveTimeListener");
        }
    }

    public void onTimeSet(TimePicker view, int hour, int minute) {
        callback.onTimeSave(hour, minute);
    }
}