package com.dyrectapp.dyrect;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class PrimaryTaskActivity extends AppCompatActivity implements
        DatePickerFragment.SaveDateListener,
        TimePickerFragment.SaveTimeListener,
        AddCategoryDialogFragment.SaveNewCategory{

    private static final String TAG = "PRIMARY_TASK_ACTIVITY";

    static final String TIME_TO_UPDATE = "TIME_TO_UPDATE";
    static final String DATE_TO_UPDATE = "DATE_TO_UPDATE";
    static final String DATE_TO_LOAD = "DATE_TO_LOAD";
    static final String TIME_TO_LOAD = "TIME_TO_LOAD";

    static final String EMPTY_NAME = "Goal name can't be empty.";
    static final String INVALID_DATE = "Date must be set.";
    static final String INVALID_TIME = "Time must be set.";

    static final int RESULT_DELETE = 5;

    private ArrayAdapter<String> adapter;
    private List<String> categoryList;
    private GregorianCalendar date;
    private GregorianCalendar time;
    private Goal goalToUpdate;
    private Intent starter;
    private boolean updating = false;

    /*
    Gets the Goal to update, if there is a Goal to update, sets the spinner's list of categories from the
    database, and if there is a Goal to update, it creates the update/delete menu instead of the save menu
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //TODO Remove all unnecessary actions to run on a different thread
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primary_task);

        String SAVE_TOOLBAR_TITLE = "New Goal";
        String UPDATE_TOOLBAR_TITLE = "Edit";

        starter = getIntent();
        if (starter.getSerializableExtra(MainActivity.GOAL_TO_UPDATE) != null) {
            updating = true;
        }

        Spinner categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        categoryList = Utility.createCategoryListFromDatabase(getApplicationContext());

        adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, categoryList);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        categorySpinner.setAdapter(adapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.createTaskToolbar);
        setSupportActionBar(toolbar);


        if(updating) {
            goalToUpdate = (Goal) starter.getSerializableExtra(MainActivity.GOAL_TO_UPDATE);

            if (goalToUpdate != null) {
                loadGoalToUpdate(goalToUpdate);
            }
            toolbar.inflateMenu(R.menu.update_task_activity_menu);
            getSupportActionBar().setTitle(UPDATE_TOOLBAR_TITLE);
        }
        else {
            toolbar.inflateMenu(R.menu.save_task_activity_menu);
            getSupportActionBar().setTitle(SAVE_TOOLBAR_TITLE);
        }
    }

    /*
    If the reason the activity is live is to update a Goal, show the update/delete menu, otherwise
    show the save menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (updating) {
            getMenuInflater().inflate(R.menu.update_task_activity_menu, menu);
        }
        else {
            getMenuInflater().inflate(R.menu.save_task_activity_menu, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case (R.id.saveMenuButton):
                Goal newGoal = collectGoalInformation();
                if (newGoal != null) {
                    Intent newGoalIntent = new Intent();
                    newGoalIntent.putExtra(MainActivity.RESULT, newGoal);
                    setResult(RESULT_OK, newGoalIntent);
                    finish();
                }
                break;

            case (R.id.updateMenuButton):
                Goal updatedGoal = collectGoalInformation(goalToUpdate);
                if (updatedGoal != null) {
                    Log.d(TAG, updatedGoal.toString());
                    Intent updateGoalIntent = new Intent();
                    updateGoalIntent.putExtra(MainActivity.RESULT, updatedGoal);
                    setResult(RESULT_OK, updateGoalIntent);
                }
                finish();
                break;

            case (R.id.deleteMenuButton):
                //TODO Create dialog to ask if you want to delete
                boolean result = deleteGoalFromDatabase(goalToUpdate);
                createSaveToast(result, "DELETE");

                Intent deleteGoalIntent = new Intent();
                deleteGoalIntent.putExtra(MainActivity.RESULT, goalToUpdate);
                setResult(RESULT_DELETE, deleteGoalIntent);
                finish();
                break;
        }
        return false;
    }

    /*
    Checks whether the date or time fields are null, and creates an invalidSaveToast if they are
    Returns false if they're null or true if they're not
     */
    private boolean confirmValidSaveData(EditText taskNameEditText) {
        if (TextUtils.isEmpty(taskNameEditText.getText().toString())) {
            createInvalidSaveDataToast(PrimaryTaskActivity.EMPTY_NAME);
            return false;
        }
        else if (date == null) {
            createInvalidSaveDataToast(PrimaryTaskActivity.INVALID_DATE);
            return false;
        }
        else if (time == null) {
            createInvalidSaveDataToast(PrimaryTaskActivity.INVALID_TIME);
            return false;
        }
        return true;
    }

    /*
    Creates a toast with a given message to say that some data is invalid when trying to save a
    new Goal
    */
    private void createInvalidSaveDataToast(String text) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    //Given a Goal, load the Name, Notes, and Category into the PrimaryTaskActivity fields
    private void loadGoalToUpdate(Goal goalToUpdate) {
        EditText goalName = (EditText) findViewById(R.id.taskNameEditText);
        EditText notes = (EditText) findViewById(R.id.notesEditText);
        Spinner categorySpinner = (Spinner) findViewById(R.id.categorySpinner);

        ArrayAdapter adapter = (ArrayAdapter) categorySpinner.getAdapter();
        int spinnerPosition = adapter.getPosition(goalToUpdate.getCategory());

        categorySpinner.setSelection(spinnerPosition);
        goalName.setText(goalToUpdate.getGoalName());
        notes.setText(goalToUpdate.getNotes());
    }

    /*
    Collects the information that was entered and stores them in a Goal object
    Returns the Goal if it's not null, or null if it is
     */
    private Goal collectGoalInformation() {
        Goal goal = new Goal();

        EditText taskNameEditText = (EditText) findViewById(R.id.taskNameEditText);
        Spinner categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        EditText notesEditText = (EditText) findViewById(R.id.notesEditText);

        if (!confirmValidSaveData(taskNameEditText)) {
            return null;
        }

        goal.setGoalName(taskNameEditText.getText().toString());

        GregorianCalendar dateAndTime = new GregorianCalendar(
                date.get(Calendar.YEAR),
                date.get(Calendar.MONTH),
                date.get(Calendar.DAY_OF_MONTH),
                time.get(Calendar.HOUR_OF_DAY),
                time.get(Calendar.MINUTE)
        );
        goal.setDueDate(dateAndTime);

        goal.setCategory(categorySpinner.getSelectedItem().toString());

        goal.setNotes(notesEditText.getText().toString());

        if (updating) {
            Goal updatingGoal = (Goal) starter.getSerializableExtra(MainActivity.GOAL_TO_UPDATE);
            goal.setID(updatingGoal.getID());
        }

        boolean result = (updating) ? updateGoalIntoDatabase(goal) : saveGoalToDatabase(goal);
        createSaveToast(result, null);

        return result ? goal : null;
    }

    /*
    Overloaded method to check for invalid information (missing date/time) on an update call. If
    there is information that is missing, then it will set the date or time to the original date
    or time, then it calls the main collectGoalInformation
    */
    private Goal collectGoalInformation(Goal originalGoal) {
        if (date == null) {
            int originalYear = originalGoal.getDueDate().get(Calendar.YEAR);
            int originalMonth = originalGoal.getDueDate().get(Calendar.MONTH);
            int originalDayOfMonth = originalGoal.getDueDate().get(Calendar.DAY_OF_MONTH);

            date = new GregorianCalendar(originalYear, originalMonth, originalDayOfMonth);
        }

        if (time == null) {
            int originalHourOfDay = originalGoal.getDueDate().get(Calendar.HOUR_OF_DAY);
            int originalMinute = originalGoal.getDueDate().get(Calendar.MINUTE);

            time = new GregorianCalendar();

            time.set(Calendar.HOUR_OF_DAY, originalHourOfDay);
            time.set(Calendar.MINUTE, originalMinute);
        }
        return collectGoalInformation();
    }

    //TODO Channge to snackbar
    //Creates a toast depending on the result and what the process is (save, delete, update)
    private void createSaveToast(boolean result, String process) {
        Context context = getApplicationContext();
        String text;

        if (result) {
            if (process == null && updating) {
                text = "Update succeeded";
            }
            else if (process != null && process.equals("DELETE")) {
                text = "Delete succeeded";
            }
            else {
                text = "Save succeeded";
            }
        }
        else {
            if (process == null && updating) {
                text = "Update failed";
            }
            else if (process != null && process.equals("DELETE")) {
                text = "Delete failed";
            }
            else {
                text = "Save failed";
            }
        }
        Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        toast.show();
    }

    /*
    Saves a Goal to the database
    Returns whether the ID of the new URI is valid or not
     */
    private boolean saveGoalToDatabase(Goal goal) {
        ContentValues values = Utility.createContentValuesFromGoal(goal);

        Uri uri = getContentResolver().insert(GoalsContract.Goals.CONTENT_URI, values);

        return (Integer.valueOf(uri.getLastPathSegment()) != -1);
    }

    /*
    Updates a Goal in the database, given the Goal
    Returns whether it actually updated anything
     */
    private boolean updateGoalIntoDatabase(Goal goal) {
        ContentValues values = Utility.createContentValuesFromGoal(goal);

        int goalID = goal.getID();
        Uri goalUri = ContentUris.withAppendedId(GoalsContract.Goals.CONTENT_URI, goalID);

        int count = getContentResolver().update(goalUri, values, null, null);

        return (count > 0);
    }

    /*
    Deletes a Goal from the database, given the Goal
    Returns whether it actually deleted anything
    */
    private boolean deleteGoalFromDatabase(Goal goal) {
        int toDeleteID = goal.getID();
        Uri toDeleteUri = ContentUris.withAppendedId(GoalsContract.Goals.CONTENT_URI, toDeleteID);

        int count = getContentResolver().delete(toDeleteUri, null, null);

        return (count > 0);
    }

    //Save the date from the DatePicker fragment for later use
    public void onDateSave(int day, int month, int year) {
        date = new GregorianCalendar(year, month, day);
    }

    //Save the time from the TimePicker fragment for later use
    public void onTimeSave(int hour, int minute) {
        time = new GregorianCalendar();

        time.set(Calendar.HOUR_OF_DAY, hour);
        time.set(Calendar.MINUTE, minute);
    }

    /*
    Public method to get new category to save from AddCategoryDialogFragment

    This method only has a single call to a private method because external classes should not be
    able to save categories to the database
    */
    public void addCategoryDialogPositiveClick(String newCategory) {
        saveCategoryToDatabase(newCategory);
    }

    /*
    Private method to save a given category to the database
    Returns whether or not the save was successful
    */
    private boolean saveCategoryToDatabase(String category) {
        ContentValues values = Utility.createContentValuesFromCategory(category);

        Uri uri = getContentResolver().insert(CategoriesContract.Categories.CONTENT_URI, values);
        int newRowID = Integer.valueOf(uri.getLastPathSegment());

        if (newRowID != -1) {
            categoryList.add(category);
            adapter.notifyDataSetChanged();

            Spinner categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
            ArrayAdapter adapter = (ArrayAdapter) categorySpinner.getAdapter();
            int spinnerPosition = adapter.getPosition(category);
            categorySpinner.setSelection(spinnerPosition);
        }

        return (Integer.valueOf(uri.getLastPathSegment()) != -1);
    }

    //Display a date picker when the setDate button is clicked
    public void showDatePickerDialog(View view) {
        DialogFragment newFragment = new DatePickerFragment();

        if (goalToUpdate != null || date != null) {
            Bundle arguments = new Bundle();
            String isolatedDate;

            if (date != null) {
                isolatedDate = Utility.isolateDate(Utility.formatDateGregorianToString(date));
                arguments.putString(PrimaryTaskActivity.DATE_TO_LOAD, isolatedDate);
            }
            else {
                isolatedDate = Utility.isolateDate(Utility.formatFullGregorianToString(goalToUpdate.getDueDate(), false));
                arguments.putString(PrimaryTaskActivity.DATE_TO_UPDATE, isolatedDate);
            }
            newFragment.setArguments(arguments);
        }
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    //Display a time picker when the setTime button is clicked
    public void showTimePickerDialog(View view) {
        DialogFragment newFragment = new TimePickerFragment();

        if (goalToUpdate != null || time != null) {
            Bundle arguments = new Bundle();
            String isolatedTime;
            if (time != null) {
                isolatedTime = Utility.isolateTime(Utility.formatTimeGregorianToString(time));
                arguments.putString(PrimaryTaskActivity.TIME_TO_LOAD, isolatedTime);
            }
            else {
                isolatedTime = Utility.isolateTime(Utility
                        .formatFullGregorianToString(goalToUpdate.getDueDate(), false));
                arguments.putString(PrimaryTaskActivity.TIME_TO_UPDATE, isolatedTime);
            }
            newFragment.setArguments(arguments);
        }
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    //Display a custom display to add extra categories when the addCategory button is clicked
    public void showAddCategoryDialog(View view) {
        DialogFragment newFragment = new AddCategoryDialogFragment();
        newFragment.show(getSupportFragmentManager(), "addCategory");
    }

    //Method called when the notesEditText is clicked to open the keyboard
    public void openKeyboard(View view) {
        EditText notesEditText = (EditText) findViewById(R.id.notesEditText);
        notesEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.SHOW_IMPLICIT);
    }
}