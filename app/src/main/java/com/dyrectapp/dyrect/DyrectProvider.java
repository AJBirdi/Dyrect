package com.dyrectapp.dyrect;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class DyrectProvider extends ContentProvider{

    private OpenDbHelper dbHelper;
    private SQLiteDatabase db;

    private final String TAG = "DYRECT_PROVIDER";
    public final static String AUTHORITY = "com.dyrectapp.dyrect.provider";

    private static final int GOALS = 1;
    private static final int GOALS_ID = 2;
    private static final int CATEGORIES = 3;
    private static final int CATEGORIES_ID = 4;

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(AUTHORITY, "goals", GOALS);
        uriMatcher.addURI(AUTHORITY, "goals/#", GOALS_ID);

        uriMatcher.addURI(AUTHORITY, "categories", CATEGORIES);
        uriMatcher.addURI(AUTHORITY, "categories/#", CATEGORIES_ID);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new OpenDbHelper(getContext());
        return true;
    }

    //TODO Break down query method into smaller methods?
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String tableName;
        int uriNum = uriMatcher.match(uri);

        switch (uriNum) {
            //Case 1 is for the entire Goals table
            case 1:
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = "_ID ASC";
                }
                break;

            //Case 2 is for a specific row in the Goals table
            case 2:
                if (selection != null) {
                    throw new IllegalArgumentException("Too many arguments. " +
                            "There should not be any selection terms when querying a specific Goal.");
                }
                else {
                    selection = "_ID = " + uri.getLastPathSegment();
                }
                break;

            //Case 3 is for the entire Categories table
            case 3:
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = "_ID ASC";
                }
                break;

            //Case 4 is for a specific row in the Categories table
            case 4:
                if (selection != null) {
                    throw new IllegalArgumentException("Too many arguments. " +
                            "There should not be any selection terms when querying a specific Category.");
                }
                else {
                    selection = "_ID = " + uri.getLastPathSegment();
                }
                break;

            default:
                throw new IllegalArgumentException("URI not found.");
        }

        if(uriNum == 1 || uriNum == 2) {
            tableName = GoalsContract.Goals.TABLE_NAME;
        }
        else {
            tableName = CategoriesContract.Categories.TABLE_NAME;
        }

        db = dbHelper.getReadableDatabase();
        dbHelper.onCreate(db);

        Cursor cursor = db.query(
                tableName,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
        return cursor;
    }

    @Override
    public Uri insert (Uri uri, ContentValues values) {
        String tableName;

        switch (uriMatcher.match(uri)) {
            //Case 1 is for the entire Goals table
            case 1:
                tableName = GoalsContract.Goals.TABLE_NAME;
                break;
            //Case 3 is for the entire Categories table
            case 3:
                tableName = CategoriesContract.Categories.TABLE_NAME;
                break;

            default:
                throw new IllegalArgumentException("URI is not found or is for a specific row.");
        }

        db = dbHelper.getWritableDatabase();
        dbHelper.onCreate(db);

        long rowID = db.insert(tableName, null, values);

        Uri rowURI = ContentUris.withAppendedId(uri, rowID);
        return rowURI;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Object[] variables = updateOrDelete(uri, selection, selectionArgs, "UPDATE");

        //Return 0 since the entire categories table can't be updated because of the UNIQUE constraint
        if (variables[0] == "") {
            return 0;
        }
        else {
            String tableName = (String) variables[0];
            selection = (String) variables[1];
            selectionArgs = (String[]) variables[2];

            db = dbHelper.getWritableDatabase();
            dbHelper.onCreate(db);

            int count = db.update(tableName, values, selection, selectionArgs);

            getContext().getContentResolver().notifyChange(uri, null);
            return count;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Object[] variables = updateOrDelete(uri, selection, selectionArgs, "DELETE");

        String tableName = (String) variables[0];
        selection = (String) variables[1];
        selectionArgs = (String[]) variables[2];

        db = dbHelper.getWritableDatabase();
        dbHelper.onCreate(db);

        int count = db.delete(tableName, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case 1:
                return "vnd.android.cursor.dir/vnd.com.dyrectapp.dyrect.provider.goals";

            case 2:
                return "vnd.android.cursor.item/vnd.com.dyrectapp.dyrect.provider.goals";

            case 3:
                return "vnd.android.cursor.dir/vnd.com.dyrectapp.dyrect.provider.categories";

            case 4:
                return "vnd.android.cursor.item/vnd.com.dyrectapp.dyrect.provider.categories";

            default:
                return "";
        }
    }

    private Object[] updateOrDelete(Uri uri, String selection, String[] selectionArgs, String process) {
        String tableName;
        int uriNum = uriMatcher.match(uri);

        //Return this because the entire Categories table can't be updated because of the UNIQUE constraint
        if (uriNum == 3 && process.equals("UPDATE")) {
            return new Object[] {""};
        }

        if(uriNum == 1 || uriNum == 2) {
            tableName = GoalsContract.Goals.TABLE_NAME;
        }
        else {
            tableName = CategoriesContract.Categories.TABLE_NAME;
        }

        if (uriNum == 2 || uriNum == 4) {
            if (selection != null) {
                throw new IllegalArgumentException("Too many arguments. " +
                        "There should not be any selection terms when querying a specific Goal or Category.");
            }
            else {
                selection = " _ID = ?";
            }
            selectionArgs = new String[] {uri.getLastPathSegment()};
        }

        Object[] variables = {tableName, selection, selectionArgs};
        return variables;
    }
}