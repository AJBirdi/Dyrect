package com.dyrectapp.dyrect;

import android.net.Uri;
import android.provider.BaseColumns;

class CategoriesContract {

    private CategoriesContract() {}

    static class Categories implements BaseColumns{
        static final String TABLE_NAME = "categories";
        static final String COLUMN_NAME_NAME = "name";

        static final Uri CONTENT_URI = Uri.parse("content://" + DyrectProvider.AUTHORITY + "/" + TABLE_NAME);

        static final String SQL_CREATE_CATEGORIES =
         "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
         _ID + " INTEGER PRIMARY KEY, " +
         COLUMN_NAME_NAME + " TEXT NOT NULL UNIQUE);" ;

        static final String SQL_DELETE_CATEGORIES =
                "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
