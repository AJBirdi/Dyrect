package com.dyrectapp.dyrect;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

class Utility {

    private final static String TAG = "UTILITY";

    //Takes a GregorianCalendar, formats the data into a string, then returns it
    static String formatFullGregorianToString(GregorianCalendar gregorianDueDate, boolean forDisplay) {
        StringBuilder stringDueDate = new StringBuilder("");

        int hourOfDay;
        int minute = gregorianDueDate.get(Calendar.MINUTE);
        int dayOfMonth = gregorianDueDate.get(Calendar.DAY_OF_MONTH);
        int month;
        String noonDesignator = "";

        if (forDisplay) {
            String[] variables = getDisplayHour(gregorianDueDate.get(Calendar.HOUR_OF_DAY));

            month = Utility.getDisplayMonth(gregorianDueDate.get(Calendar.MONTH));
            hourOfDay = Integer.parseInt(variables[0]);
            noonDesignator = variables[1];
        }
        else {
            month = gregorianDueDate.get(Calendar.MONTH);
            hourOfDay = gregorianDueDate.get(Calendar.HOUR_OF_DAY);
        }

        //If the month is less than 10, add a leading zero
        if(month < 10) {
            stringDueDate.append("0" + month);
        }
        else {
            stringDueDate.append(month);
        }
        stringDueDate.append("/");

        //If the day of the month is less than 10, add a leading zero
        if(dayOfMonth < 10) {
            stringDueDate.append("0" + dayOfMonth);
        }
        else {
            stringDueDate.append(dayOfMonth);
        }

        //Add a backslash then the year
        stringDueDate.append("/");
        stringDueDate.append(gregorianDueDate.get(Calendar.YEAR));
        stringDueDate.append(" ");

        //If the hour is less than 10, add a leading 0
        //Also keeps the date in 24 hour form (ex: 18:00 instead of 06:00) to track whether the time
        // is before or after noon without another indicator
        if(hourOfDay < 10) {
            stringDueDate.append("0" + hourOfDay);
        }
        else {
            stringDueDate.append(hourOfDay);
        }
        stringDueDate.append(":");

        //If the minute is less than 10, add a leading 0
        if(minute < 10) {
            stringDueDate.append("0" + minute);
        }
        else {
            stringDueDate.append(minute);
        }

        if (forDisplay) {
            stringDueDate.append(" " + noonDesignator);
        }
        return stringDueDate.toString();
    }

    //Takes a String, formats it into a GregorianCalendar, then returns it
    static GregorianCalendar formatStringToGregorian(String stringDueDate) {
        StringBuilder gregorianDueDate = new StringBuilder(stringDueDate);
        GregorianCalendar gregorianCalendar;

        int month = Integer.parseInt(gregorianDueDate.substring(0, 2));
        int dayOfMonth = Integer.parseInt(gregorianDueDate.substring(3, 5));
        int year = Integer.parseInt(gregorianDueDate.substring(6, 10));

        int hourOfDay = Integer.parseInt(gregorianDueDate.substring(11, 13));
        int minute = Integer.parseInt(gregorianDueDate.substring(14));

        gregorianCalendar = new GregorianCalendar(
                year,
                month,
                dayOfMonth,
                hourOfDay,
                minute
        );

        return gregorianCalendar;
    }

    /*
    Takes a GregorianCalendar with only the date, adds a dummy time, then calls the main
    GregorianToString method
     */
    static String formatDateGregorianToString(GregorianCalendar gregorianDate) {
        gregorianDate.set(Calendar.HOUR_OF_DAY, 0);
        gregorianDate.set(Calendar.MINUTE, 0);

        return formatFullGregorianToString(gregorianDate, false);
    }

    /*
    Takes a GregorianCalendar with only the time, adds a dummy date, then calls the main
    GregorianToString method
    */
    static String formatTimeGregorianToString(GregorianCalendar gregorianTime) {
        gregorianTime.set(Calendar.MONTH, 0);
        gregorianTime.set(Calendar.YEAR, 1970);
        gregorianTime.set(Calendar.DAY_OF_MONTH, 1);
        return formatFullGregorianToString(gregorianTime, false);
    }

    /*
     Given a cursor of Goal objects and a TAG from the calling process, log the Goal's name,
     category, date, and ID
     */
    static void logGoalFromCursor (Cursor cursor, String TAG) {
        String name = cursor.getString(cursor.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME));
        String category = cursor.getString(cursor.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_CATEGORY));
        String date = cursor.getString(cursor.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME));
        int id = cursor.getInt(cursor.getColumnIndex(GoalsContract.Goals._ID));

        Log.d(
                TAG, "\n" + name
                + "\n" + category
                + "\n" + date
                + "\n" + id
        );
    }


    //Given a cursor of categories and a TAG from the calling process, log the Categories' name and ID
    static void logCategoryFromCursor(Cursor cursor, String TAG) {
        String name = cursor.getString(cursor.getColumnIndex(CategoriesContract.Categories.COLUMN_NAME_NAME));
        int ID = cursor.getInt(cursor.getColumnIndex(CategoriesContract.Categories._ID));
    }

    //Given a String, convent it into a ContentValues object then return it
    static ContentValues createContentValuesFromCategory(String categoryName) {
        ContentValues values = new ContentValues();
        values.put(CategoriesContract.Categories.COLUMN_NAME_NAME, categoryName);
        return values;
    }

    //Given a Goal object, convert it into a ContentValues object, then return it
    static ContentValues createContentValuesFromGoal(Goal goal) {
        ContentValues values = new ContentValues();

        String formattedDateAndTime = Utility.formatFullGregorianToString(goal.getDueDate(), false);

        values.put(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME, goal.getGoalName());
        values.put(GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME, formattedDateAndTime);
        values.put(GoalsContract.Goals.COLUMN_NAME_CATEGORY, goal.getCategory());
        values.put(GoalsContract.Goals.COLUMN_NAME_NOTES, goal.getNotes());

        return values;
    }

    //Given a cursor with a single Goal, create a Goal object, then return it
    static Goal createGoalFromCursor(Cursor cursor) {
        Goal goal = new Goal();

        String name;
        String dateAndTime;
        String category;
        String notes;
        int id;

        //Log.d("UTILITY", );
        name = cursor.getString(cursor.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME));
        dateAndTime = cursor.getString(cursor.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME));
        category = cursor.getString(cursor.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_CATEGORY));
        notes = cursor.getString(cursor.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_NOTES));
        id = cursor.getInt(cursor.getColumnIndex(GoalsContract.Goals._ID));

        goal.setGoalName(name);
        goal.setDueDate(formatStringToGregorian(dateAndTime));
        goal.setCategory(category);
        goal.setNotes(notes);
        goal.setID(id);

        return goal;
    }

    /*
    Given a context from the calling process, create an ArrayList of Goals from the Goals table in
    the database
     */
    static ArrayList<Goal> createGoalListFromDatabase(Context context) {
        ArrayList<Goal> goalArrayList= new ArrayList<>();

        Cursor cursor = context.getContentResolver().query(GoalsContract.Goals.CONTENT_URI, null, null, null, null);

        if(cursor.moveToFirst()) {
            do {
                Goal goal = createGoalFromCursor(cursor);
                goalArrayList.add(goal);
            } while(cursor.moveToNext());
        }

        return goalArrayList;
    }

    /*
    Given a context from the calling process, create an ArrayList of categories from the Categories
    table in the database
     */
    static ArrayList<String> createCategoryListFromDatabase(Context context) {
        ArrayList<String> categoryList = new ArrayList<>();

        Cursor categories = context.getContentResolver().query(CategoriesContract.Categories.CONTENT_URI, null, null, null, null);

        if(categories.moveToFirst()) {
            do {
                String category = categories.getString(categories.getColumnIndex(CategoriesContract.Categories.COLUMN_NAME_NAME));
                categoryList.add(category);
            } while(categories.moveToNext());
        }

        categories.close();
        return categoryList;
    }

    //Given a String that has the full date and time, return only the time
    static String isolateTime(String dateAndTime) {
        return dateAndTime.substring(11);
    }

    //Given a String that has the full date and time, return only the date
    static String isolateDate(String dateAndTime) {
        return dateAndTime.substring(0, 10);
    }

    //Given a String that only has the time, return only the hour in 24 hour form
    static int isolateHour(String time) {
        return Integer.valueOf(time.substring(0, 2));
    }

    //Given a String that only has the time, return only the minute
    static int isolateMinute(String time) {
        return Integer.valueOf(time.substring(3));
    }

    //Given a String that only has the date, return only the month
    static int isolateMonth(String date) {
        return Integer.valueOf(date.substring(0,2));
    }

    //Given a String that only has the date, return only the day of the month
    static int isolateDay(String date) {
        return Integer.valueOf(date.substring(3,5));
    }

    //Given a String that only has the date, return only the year
    static int isolateYear(String date) {
        return Integer.valueOf(date.substring(6));
    }

    /*
    Converts the Gregorian month to a month that makes follows the 1-12 month scheme, instead of 0-11,
    like normal people do
    */

    static String formatForDateDisplay(String formattedForDisplay) {
        return formattedForDisplay.substring(0, 10);
    }

    static String formatForTimeDisplay(String formattedForDisplay) {
        return formattedForDisplay.substring(11);
    }

    private static int getDisplayMonth (int gregorianMonth) {
        return gregorianMonth + 1;
    }

    /*
    Converts the Gregorian time to a time that is in 12 hour format, instead of 24 hour format
     */
    private static String[] getDisplayHour (int hour) {
        int displayHour = hour;
        String noonDesignator = "AM";

        if (hour == 0) {
            displayHour = 12;
            hour = 12;
        }
        else if (hour == 13) {
            displayHour = 1;
        }

        if (hour >= 13) {
            displayHour = hour % 12;
            noonDesignator = "PM";
        }
        return new String[] {String.valueOf(displayHour), noonDesignator};
    }
}