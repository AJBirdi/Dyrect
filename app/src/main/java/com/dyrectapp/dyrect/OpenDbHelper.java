package com.dyrectapp.dyrect;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;

//TODO AsyncTask implementation

final class OpenDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "GoalsAndCategories.db";

    private final String TAG = "OPEN_DB_HELPER";

    private Context starterContext;

    OpenDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        starterContext = context;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(GoalsContract.Goals.SQL_CREATE_GOALS);
        db.execSQL(CategoriesContract.Categories.SQL_CREATE_CATEGORIES);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(starterContext);

        if(!prefs.getBoolean("firstTime", false)) {
            //TODO Do on a different thread
            addDefaultData(db);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("firstTime", true);
            editor.apply();
        }
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(GoalsContract.Goals.SQL_DELETE_GOALS);
        db.execSQL(CategoriesContract.Categories.SQL_DELETE_CATEGORIES);
        onCreate(db);
    }

    private void addDefaultData(SQLiteDatabase db) {
        String[] defaultCategories = {"Work", "Fitness", "Family", "Finance"};

        for(String currentName : defaultCategories) {
            ContentValues values = new ContentValues();
            values.put(CategoriesContract.Categories.COLUMN_NAME_NAME, currentName);
            db.insert(CategoriesContract.Categories.TABLE_NAME, null, values);
        }

        Goal loseWeight = new Goal();
        loseWeight.setGoalName("Lose weight");
        loseWeight.setDueDate(Utility.formatStringToGregorian("05/19/2017 00:00"));
        loseWeight.setCategory("Fitness");
        loseWeight.setNotes("");

        Goal doTaxes = new Goal();
        doTaxes.setGoalName("Do my taxes");
        doTaxes.setDueDate(Utility.formatStringToGregorian("04/01/2018 00:00"));
        doTaxes.setCategory("Work");
        doTaxes.setNotes("Hire accountant.");

        ContentValues values = Utility.createContentValuesFromGoal(loseWeight);
        db.insert(GoalsContract.Goals.TABLE_NAME, null, values);

        values = Utility.createContentValuesFromGoal(doTaxes);
        db.insert(GoalsContract.Goals.TABLE_NAME, null, values);
    }
}