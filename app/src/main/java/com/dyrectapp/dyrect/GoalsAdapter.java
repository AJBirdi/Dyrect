package com.dyrectapp.dyrect;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

class GoalsAdapter extends RecyclerView.Adapter<GoalsAdapter.ViewHolder>{

    private final String TAG = "GOALS_ADAPTER";

    private List<Goal> goalsList;
    private Context context;
    private UpdateExistingGoalListener updateExistingGoal;

    interface UpdateExistingGoalListener {
        void updateGoal(int adapterPosition);
    }

    private Context getContext() {
        return context;
    }

    GoalsAdapter(Context context, List<Goal> goalsList) {
        this.context = context;
        this.goalsList = goalsList;

    }

    @Override
    public GoalsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View goalView = inflater.inflate(R.layout.item_goal, parent, false);

        return new ViewHolder(goalView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Goal goal = goalsList.get(position);

        TextView name = holder.itemGoalName;
        TextView category = holder.itemGoalCategory;
        TextView date = holder.itemGoalDate;
        TextView time = holder.itemGoalTime;

        String formattedForDisplay = Utility.formatFullGregorianToString(goal.getDueDate(), true);
        String formattedDateForDisplay = Utility.formatForDateDisplay(formattedForDisplay);
        String formattedTimeForDisplay = Utility.formatForTimeDisplay(formattedForDisplay);

        name.setText(goal.getGoalName());
        category.setText(goal.getCategory());
        date.setText(formattedDateForDisplay);
        time.setText(formattedTimeForDisplay);

        holder.updateExistingGoalListener = updateExistingGoal;
    }

    void setUpdateExistingGoalListener(UpdateExistingGoalListener updateExistingGoal) {
        this.updateExistingGoal = updateExistingGoal;
    }

    @Override
    public int getItemCount() {
        return goalsList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView itemGoalName;
        TextView itemGoalCategory;
        TextView itemGoalDate;
        TextView itemGoalTime;
        UpdateExistingGoalListener updateExistingGoalListener;

        ViewHolder(View itemView) {
            super(itemView);

            itemGoalName = (TextView) itemView.findViewById(R.id.itemGoalNameTextView);
            itemGoalCategory = (TextView) itemView.findViewById(R.id.itemGoalCategoryTextView);
            itemGoalDate = (TextView) itemView.findViewById(R.id.itemGoalDateTextView);
            itemGoalTime = (TextView) itemView.findViewById(R.id.itemGoalTimeTextView);

            itemView.setOnClickListener(itemClicked);
        }

        View.OnClickListener itemClicked = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (updateExistingGoalListener != null) {
                    updateExistingGoalListener.updateGoal(getAdapterPosition());
                }
            }
        };
    }
}