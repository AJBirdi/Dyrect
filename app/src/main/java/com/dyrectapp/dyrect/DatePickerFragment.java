package com.dyrectapp.dyrect;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.TextView;

import java.util.Calendar;
import java.util.IllegalFormatCodePointException;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private SaveDateListener callback;
    private final String TAG = "DATE_PICKER_FRAGMENT";

    public DatePickerFragment() {

    }

    interface SaveDateListener {
        void onDateSave(int day, int month, int year);
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        Bundle arguments = getArguments();

        if (arguments != null) {
            String dateToUpdate;

            if (arguments.get(PrimaryTaskActivity.DATE_TO_LOAD) != null) {
                dateToUpdate = (String) arguments.get(PrimaryTaskActivity.DATE_TO_LOAD);
            }
            else {
                dateToUpdate = (String) arguments.get(PrimaryTaskActivity.DATE_TO_UPDATE);
            }
            month = Utility.isolateMonth(dateToUpdate);
            year = Utility.isolateYear(dateToUpdate);
            day = Utility.isolateDay(dateToUpdate);
        }
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity = (Activity) context;
        try {
            callback = (SaveDateListener) activity;
        }
        catch(ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement SaveDateListener");
        }
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        callback.onDateSave(day, month, year);
    }
}