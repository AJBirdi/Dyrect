package com.dyrectapp.dyrect;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class AddCategoryDialogFragment extends DialogFragment {

    private final String TAG = "ADD_CATEGORY_DIALOG_FRAGMENT";

    SaveNewCategory callback;
    private EditText saveCategoryEditText;

    public AddCategoryDialogFragment() {}

    interface SaveNewCategory {
        void addCategoryDialogPositiveClick(String category);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_category_dialog, container, false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getContext());

        //Custom view
        final View textEntryView = inflater.inflate(R.layout.dialog_add_category, null);
        builder.setView(textEntryView)
        .setPositiveButton(R.string.ok_dialog_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                saveCategoryEditText = (EditText) textEntryView.findViewById(R.id.addCategoryEditText);
                String newCategory = saveCategoryEditText.getText().toString();
                if (!TextUtils.isEmpty(newCategory)) {
                    callback.addCategoryDialogPositiveClick(newCategory);
                }
            }
        })
        .setNegativeButton(R.string.cancel_dialog_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do nothing
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;

        try {
            callback = (SaveNewCategory) activity;
        }
        catch(ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement SaveNewCategory");
        }
    }
}
