package com.dyrectapp.dyrect;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.MatrixCursor;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.core.deps.guava.collect.ArrayListMultimap;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class UtilityInstrumentedTest {

    String[] columnNames = {
            GoalsContract.Goals.COLUMN_NAME_GOAL_NAME,
            GoalsContract.Goals.COLUMN_NAME_CATEGORY,
            GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME,
            GoalsContract.Goals.COLUMN_NAME_NOTES,
            GoalsContract.Goals._ID
    };

    Context context;

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getContext();
    }

    public Context getContext() {
        return context;
    }

    @Test
    public void TestCategoryToContentValues() {
        String category = "New Category";

        ContentValues values = Utility.createContentValuesFromCategory(category);
        Assert.assertEquals(values.get(CategoriesContract.Categories.COLUMN_NAME_NAME), category);
    }

    @Test
    public void TestCreateContentValuesFromGoal() {
        Goal goal = new Goal();
        goal.setGoalName("Goal Name");
        goal.setCategory("Work");
        goal.setDueDate(Utility.formatStringToGregorian("05/20/2017 10:30"));
        goal.setNotes("");

        ContentValues values = Utility.createContentValuesFromGoal(goal);
        Assert.assertEquals(values.get(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME), "Goal Name");
        Assert.assertEquals(values.get(GoalsContract.Goals.COLUMN_NAME_CATEGORY), "Work");
        Assert.assertEquals(values.get(GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME), "05/20/2017 10:30");
        Assert.assertEquals(values.get(GoalsContract.Goals.COLUMN_NAME_NOTES), "");
    }

    @Test
    public void TestCreateGoalFromCursor() {
        MatrixCursor matrixCursor = new MatrixCursor(columnNames);

        Object[] goalObjects = {
                "Goal Name",
                "Work",
                "05/20/2017 10:30",
                "",
                -1
        };

        matrixCursor.addRow(goalObjects);
        matrixCursor.moveToFirst();
        Goal goal = Utility.createGoalFromCursor(matrixCursor);

        Assert.assertEquals(goal.getGoalName(), "Goal Name");
        Assert.assertEquals(goal.getCategory(), "Work");
        Assert.assertEquals(Utility.formatFullGregorianToString(goal.getDueDate(), false), "05/20/2017 10:30");
        Assert.assertEquals(goal.getNotes(), "");
        Assert.assertEquals(goal.getID(), -1);
    }

    @Test
    public void TestCreateGoalListFromDatabase() {
        Context context = getContext();
        context.getContentResolver().delete(GoalsContract.Goals.CONTENT_URI, null, null);

        ArrayList<Goal> goalArrayList = Utility.createGoalListFromDatabase(context);
        Assert.assertEquals(0, goalArrayList.size());

        ContentValues values = new ContentValues();
        values.put(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME, "TestCreateGoalListFromDatabase");
        values.put(GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME, "05/20/2017 16:00");
        values.put(GoalsContract.Goals.COLUMN_NAME_CATEGORY, "Work");
        values.put(GoalsContract.Goals.COLUMN_NAME_NOTES, "");

        context.getContentResolver().insert(GoalsContract.Goals.CONTENT_URI, values);

        values.put(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME, "TestCreateGoalListFromDatabase2");
        values.put(GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME, "05/20/2017 16:00");
        values.put(GoalsContract.Goals.COLUMN_NAME_CATEGORY, "Work2");
        values.put(GoalsContract.Goals.COLUMN_NAME_NOTES, "2");

        context.getContentResolver().insert(GoalsContract.Goals.CONTENT_URI, values);

        goalArrayList = Utility.createGoalListFromDatabase(context);
        Assert.assertEquals(2, goalArrayList.size());
        Goal goal1 = goalArrayList.get(0);
        Goal goal2 = goalArrayList.get(1);

        Assert.assertEquals("TestCreateGoalListFromDatabase", goal1.getGoalName());
        Assert.assertEquals("05/20/2017 16:00", Utility.formatFullGregorianToString(goal1.getDueDate(), false));
        Assert.assertEquals("Work", goal1.getCategory());
        Assert.assertEquals("", goal1.getNotes());

        Assert.assertEquals("TestCreateGoalListFromDatabase2", goal2.getGoalName());
        Assert.assertEquals("05/20/2017 16:00", Utility.formatFullGregorianToString(goal2.getDueDate(), false));
        Assert.assertEquals("Work2", goal2.getCategory());
        Assert.assertEquals("2", goal2.getNotes());
    }

    @Test
    public void TestCreateCategoryListFromDatabase() {
        Context context = getContext();
        context.getContentResolver().delete(CategoriesContract.Categories.CONTENT_URI, null, null);

        ArrayList<String> categoryList = Utility.createCategoryListFromDatabase(context);
        Assert.assertEquals(0, categoryList.size());

        ContentValues values = new ContentValues();

        values.put(CategoriesContract.Categories.COLUMN_NAME_NAME, "TestCreateCategoryListFromDatabase");
        context.getContentResolver().insert(CategoriesContract.Categories.CONTENT_URI, values);

        values.put(CategoriesContract.Categories.COLUMN_NAME_NAME, "TestCreateCategoryListFromDatabase2");
        context.getContentResolver().insert(CategoriesContract.Categories.CONTENT_URI, values);

        categoryList = Utility.createCategoryListFromDatabase(context);
        Assert.assertEquals(2, categoryList.size());

        Assert.assertEquals("TestCreateCategoryListFromDatabase", categoryList.get(0));
        Assert.assertEquals("TestCreateCategoryListFromDatabase2", categoryList.get(1));
    }
}
