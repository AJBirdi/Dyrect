package com.dyrectapp.dyrect;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

public class MainActivityInstrumetedTest {

    private Context context;
    private Intent intent;
    private Goal goal;

    public void setIntentData() {
        goal = new Goal();
        intent = new Intent();

        goal.setGoalName("Goal");
        goal.setDueDate(Utility.formatStringToGregorian("05/20/2017 10:30"));
        goal.setCategory("Work");
        goal.setNotes("");

        intent.putExtra("DATA", goal);
    }

    @Rule
    ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getContext();
    }

    @Test
    public void TestOnActivityResultOk() {
        activityTestRule.getActivity().onActivityResult(
                MainActivity.CREATE_GOAL_REQUEST, Activity.RESULT_OK, intent);


    }
}
