package com.dyrectapp.dyrect;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.core.deps.guava.util.concurrent.UncheckedTimeoutException;
import android.text.TextUtils;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class DyrectProviderInstrumentedTest {
    Context context;

    Goal goal1;
    Goal goal2;
    String category1;
    String category2;

    //Dump the database and add 2 goals and 2 categories
    private void resetDatabase() {
        goal1 = new Goal();
        goal2 = new Goal();
        context.getContentResolver().delete(GoalsContract.Goals.CONTENT_URI, null, null);
        context.getContentResolver().delete(CategoriesContract.Categories.CONTENT_URI, null, null);

        ContentValues values1 = new ContentValues();
        ContentValues values2 = new ContentValues();

        values1.put(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME, "Dummy Goal #1");
        values1.put(GoalsContract.Goals.COLUMN_NAME_CATEGORY, "Work");
        values1.put(GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME, "05/20/2017 10:30");
        values1.put(GoalsContract.Goals.COLUMN_NAME_NOTES, "");

        goal1.setGoalName("Dummy Goal #1");
        goal1.setCategory("Work");
        goal1.setDueDate(Utility.formatStringToGregorian("05/20/2017 10:30"));
        goal1.setNotes("");

        values2.put(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME, "Dummy Goal #2");
        values2.put(GoalsContract.Goals.COLUMN_NAME_CATEGORY, "Fitness");
        values2.put(GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME, "05/21/2017 10:30");
        values2.put(GoalsContract.Goals.COLUMN_NAME_NOTES, "");

        goal2.setGoalName("Dummy Goal #2");
        goal2.setCategory("Fitness");
        goal2.setDueDate(Utility.formatStringToGregorian("05/21/2017 10:30"));
        goal2.setNotes("");

        context.getContentResolver().insert(GoalsContract.Goals.CONTENT_URI, values1);
        context.getContentResolver().insert(GoalsContract.Goals.CONTENT_URI, values2);

        values1 = new ContentValues();
        values2 = new ContentValues();

        values1.put(CategoriesContract.Categories.COLUMN_NAME_NAME, "Dummy Category #1");
        values2.put(CategoriesContract.Categories.COLUMN_NAME_NAME, "Dummy Category #2");

        context.getContentResolver().insert(CategoriesContract.Categories.CONTENT_URI, values1);
        context.getContentResolver().insert(CategoriesContract.Categories.CONTENT_URI, values2);

        category1 = "Dummy Category #1";
        category2 = "Dummy Category #2";
    }

    @Before
    public void setUp() {
        context = InstrumentationRegistry.getContext();
        resetDatabase();
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void TestQueryGoalsTableWithNoSortOrder() {
        resetDatabase();

        Cursor goalsTable = context.getContentResolver().query(GoalsContract.Goals.CONTENT_URI, null, null, null, null);

        if (goalsTable.moveToFirst()) {
            Assert.assertEquals(goalsTable.getString(
                    goalsTable.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME)),
                    goal1.getGoalName());

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME)),
                    Utility.formatFullGregorianToString(goal1.getDueDate(), false));

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_CATEGORY)),
                    goal1.getCategory());

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_NOTES)),
                    goal1.getNotes());

            goalsTable.moveToNext();

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_GOAL_NAME)),
                    goal2.getGoalName());

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME)),
                    Utility.formatFullGregorianToString(goal2.getDueDate(), false));

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_CATEGORY)),
                    goal2.getCategory());

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_NOTES)),
                    goal2.getNotes());

            Assert.assertFalse(goalsTable.moveToNext());
        }
    }

    @Test
    public void TestQueryGoalsTableWithSortOrderDesc() {
        resetDatabase();

        Cursor goalsTable = context.getContentResolver().query(GoalsContract.Goals.CONTENT_URI, null, null, null, "_ID DESC");

        if (goalsTable.moveToFirst()) {
            Assert.assertEquals(goalsTable.getString(
                    goalsTable.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME)),
                    goal2.getGoalName());

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME)),
                    Utility.formatFullGregorianToString(goal2.getDueDate(), false));

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_CATEGORY)),
                    goal2.getCategory());

            Assert.assertEquals(goalsTable.getString(goalsTable.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_NOTES)),
                    goal2.getNotes());

            goalsTable.moveToNext();
            Assert.assertFalse(goalsTable.moveToNext());
        }
    }

    @Test
    public void TestQuerySpecificRowInGoalsTableWithNullSelection() {
        resetDatabase();

        Cursor firstGoal = context.getContentResolver().query(ContentUris.withAppendedId(GoalsContract.Goals.CONTENT_URI, 1), null, null, null, null);

        if (firstGoal.moveToFirst()) {
            Assert.assertEquals(firstGoal.getString(
                    firstGoal.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME)),
                    goal1.getGoalName());

            Assert.assertEquals(firstGoal.getString(firstGoal.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME)),
                    Utility.formatFullGregorianToString(goal1.getDueDate(), false));

            Assert.assertEquals(firstGoal.getString(firstGoal.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_CATEGORY)),
                    goal1.getCategory());

            Assert.assertEquals(firstGoal.getString(firstGoal.getColumnIndex(
                    GoalsContract.Goals.COLUMN_NAME_NOTES)),
                    goal1.getNotes());
        }
    }

    @Test
    public void TestQueryCategoriesTableWithNoSortOrder() {
        resetDatabase();

        Cursor cursor = context.getContentResolver().query(
                CategoriesContract.Categories.CONTENT_URI, null, null, null, null);

        if (cursor.moveToFirst()) {
            Assert.assertEquals(category1,
                    cursor.getString(cursor.getColumnIndex(CategoriesContract.Categories.COLUMN_NAME_NAME)));

            cursor.moveToNext();

            Assert.assertEquals(category2,
                    cursor.getString(cursor.getColumnIndex(CategoriesContract.Categories.COLUMN_NAME_NAME)));
        }
    }

    @Test
    public void TestQueryCategoriesTableWithSortOrderDesc() {
        resetDatabase();

        Cursor cursor = context.getContentResolver().query(
                CategoriesContract.Categories.CONTENT_URI, null, null, null, "_ID DESC");

        if (cursor.moveToFirst()) {
            Assert.assertEquals(category2,
                    cursor.getString(cursor.getColumnIndex(CategoriesContract.Categories.COLUMN_NAME_NAME)));

            cursor.moveToNext();

            Assert.assertEquals(category1,
                    cursor.getString(cursor.getColumnIndex(CategoriesContract.Categories.COLUMN_NAME_NAME)));
        }
    }

    @Test
    public void TestQuerySpecificRowInCategoriesTableWithNullSelection() {
        resetDatabase();

        Cursor cursor = context.getContentResolver().query(
                ContentUris.withAppendedId(CategoriesContract.Categories.CONTENT_URI, 1), null, null, null, null);

        if (cursor.moveToFirst()) {
            Assert.assertEquals(category1,
                    cursor.getString(cursor.getColumnIndex(CategoriesContract.Categories.COLUMN_NAME_NAME)));
        }
    }

    @Test
    public void TestInsertGoalsTable() {
        resetDatabase();

        ContentValues values = new ContentValues();
        values.put(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME, "Dummy goal");
        values.put(GoalsContract.Goals.COLUMN_NAME_CATEGORY, "Work");
        values.put(GoalsContract.Goals.COLUMN_NAME_DATE_AND_TIME, "05/20/2017 10:30");
        values.put(GoalsContract.Goals.COLUMN_NAME_NOTES, "");

        ArrayList<Goal> arrayList = Utility.createGoalListFromDatabase(context);

        Uri newRow = context.getContentResolver().insert(GoalsContract.Goals.CONTENT_URI, values);
        int rowId = Integer.valueOf(newRow.getLastPathSegment());

        Assert.assertEquals(rowId, arrayList.size() + 1);
    }

    @Test
    public void TestInsertCategoriesTable() {
        resetDatabase();

        ContentValues values = new ContentValues();
        values.put(CategoriesContract.Categories.COLUMN_NAME_NAME, "Dummy Category");

        ArrayList<String> arrayList = Utility.createCategoryListFromDatabase(context);

        Uri newRow = context.getContentResolver().insert(CategoriesContract.Categories.CONTENT_URI, values);
        int rowId = Integer.valueOf(newRow.getLastPathSegment());

        Assert.assertEquals(rowId, arrayList.size() + 1);
    }

    @Test
    public void TestUpdateSpecificGoal() {
        resetDatabase();

        ContentValues values = new ContentValues();
        values.put(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME, "I've changed");

        int count = context.getContentResolver().update(
                ContentUris.withAppendedId(GoalsContract.Goals.CONTENT_URI, 1), values, null, null);
        Cursor cursor = context.getContentResolver().query(
                ContentUris.withAppendedId(GoalsContract.Goals.CONTENT_URI, 1), null, null, null, null);
        cursor.moveToFirst();

        Assert.assertNotSame(0, count);
        Assert.assertEquals("I've changed",
                cursor.getString(cursor.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME)));
    }

    @Test
    public void TestUpdateGoalsTable() {
        resetDatabase();

        ContentValues values = new ContentValues();
        values.put(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME, "I've changed");

        int count = context.getContentResolver().update(
                GoalsContract.Goals.CONTENT_URI, values, null, null);
        Cursor cursor = context.getContentResolver().query(
                GoalsContract.Goals.CONTENT_URI, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                Assert.assertEquals("I've changed",
                        cursor.getString(cursor.getColumnIndex(GoalsContract.Goals.COLUMN_NAME_GOAL_NAME)));
            } while (cursor.moveToNext());
        }
        Assert.assertNotSame(0, count);
    }

    @Test
    public void TestUpdateSpecificCategory() {
        resetDatabase();

        ContentValues values = new ContentValues();
        values.put(CategoriesContract.Categories.COLUMN_NAME_NAME, "A new category");

        int count = context.getContentResolver().update(
                ContentUris.withAppendedId(CategoriesContract.Categories.CONTENT_URI, 1), values, null, null);
        Cursor cursor = context.getContentResolver().query(
                ContentUris.withAppendedId(CategoriesContract.Categories.CONTENT_URI, 1), null, null, null, null);
        cursor.moveToFirst();

        Assert.assertNotSame(0, count);
        Assert.assertEquals("A new category",
                cursor.getString(cursor.getColumnIndex(CategoriesContract.Categories.COLUMN_NAME_NAME)));
    }

    @Test
    public void TestUpdateCategoriesTable() {
        resetDatabase();

        ContentValues values = new ContentValues();
        values.put(CategoriesContract.Categories.COLUMN_NAME_NAME, "A new category");

        int count = context.getContentResolver().update(
                CategoriesContract.Categories.CONTENT_URI, values, null, null);
        Assert.assertEquals(0, count);
    }

    @Test
    public void TestDeleteSpecificGoal() {
        resetDatabase();

        context.getContentResolver().delete(
                ContentUris.withAppendedId(GoalsContract.Goals.CONTENT_URI, 1), null, null);

        ArrayList<Goal> arrayList = Utility.createGoalListFromDatabase(context);
        Assert.assertEquals(1, arrayList.size());
        Assert.assertEquals(2, arrayList.get(0).getID());
    }

    @Test
    public void TestDeleteGoalsTable() {
        resetDatabase();

        context.getContentResolver().delete(GoalsContract.Goals.CONTENT_URI, null, null);

        ArrayList<Goal> arrayList = Utility.createGoalListFromDatabase(context);
        Assert.assertEquals(0, arrayList.size());
    }

    @Test
    public void TestDeleteSpecificCategory() {
        resetDatabase();

        context.getContentResolver().delete(
                ContentUris.withAppendedId(CategoriesContract.Categories.CONTENT_URI, 1), null, null);

        ArrayList<String> arrayList = Utility.createCategoryListFromDatabase(context);
        Assert.assertEquals(1, arrayList.size());
        Assert.assertEquals(category2, arrayList.get(0));
    }

    @Test
    public void TestDeleteCategoriesTable() {
        resetDatabase();

        context.getContentResolver().delete(CategoriesContract.Categories.CONTENT_URI, null, null);

        ArrayList<String> arrayList = Utility.createCategoryListFromDatabase(context);
        Assert.assertEquals(0, arrayList.size());
    }

    @Test
    public void TestGetTypeGoalsTable() {
        String type = context.getContentResolver().getType(GoalsContract.Goals.CONTENT_URI);
        Assert.assertEquals(type, "vnd.android.cursor.dir/vnd.com.dyrectapp.dyrect.provider.goals");
    }

    @Test
    public void TestGetTypeSpecificGoal() {
        String type = context.getContentResolver().getType(ContentUris.withAppendedId(GoalsContract.Goals.CONTENT_URI, 1));
        Assert.assertEquals(type, "vnd.android.cursor.item/vnd.com.dyrectapp.dyrect.provider.goals");
    }

    @Test
    public void TestGetTypeCategoryTable() {
        String type = context.getContentResolver().getType(CategoriesContract.Categories.CONTENT_URI);
        Assert.assertEquals(type, "vnd.android.cursor.dir/vnd.com.dyrectapp.dyrect.provider.categories");
    }

    @Test
    public void TestGetTypeSpecificCategory() {
        String type = context.getContentResolver().getType(ContentUris.withAppendedId(CategoriesContract.Categories.CONTENT_URI, 1));
        Assert.assertEquals(type, "vnd.android.cursor.item/vnd.com.dyrectapp.dyrect.provider.categories");
    }

    @Test
    public void TestGetTypeDefault() {
        String type = context.getContentResolver().getType(Uri.parse("New Uri"));
        Assert.assertTrue(TextUtils.isEmpty(type));
    }
}